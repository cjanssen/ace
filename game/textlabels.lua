function initTextLabels()
    labelsfont = love.graphics.newFont("fnt/LiquidCrystal-Normal.otf",48)
    infofont = love.graphics.newFont("fnt/LiquidCrystal-Normal.otf",24)

    endtextz = 15
    -- titlesActive = true

    text = {
        delay = 1,
        timer = 0,
        spdmult = 0.35,
        list = {},
        count = 0,
        canvas = love.graphics.newCanvas(screenSize.x, screenSize.y),
    }

    initQuotes()
end

function initQuotes()
    rightbag = {}
    for _,quotedata in ipairs(secondquotes) do
        local title = quotedata[1][1]
        local year = quotedata[1][2]
        for _,txt in ipairs(quotedata[2]) do
            table.insert(rightbag, {
                title = title,
                year = year,
                txt = txt,
                })
        end
    end
end

function getNewQuote()
    local quotedata = table.remove(rightbag, math.random(#rightbag))
    if #rightbag <= 0 then
        initQuotes()
    end

    return quotedata
end

function addText()
    if generalPhase <= 6 or warptime > 4 or text.count >= 5 then return end
    local quotedata = getNewQuote()

    local ww,hh = labelsfont:getWidth(quotedata.txt),labelsfont:getHeight()
    local nlines=1
    for _ in quotedata.txt:gmatch("\n") do nlines=nlines+1 end

    local tw,yw = infofont:getWidth(quotedata.title), infofont:getWidth(quotedata.year)

    local ltext = {
        txt = quotedata.txt,
        year = quotedata.year,
        title = quotedata.title,
        x = ww*0.5 + (math.random()-0.5)*ww,
        y = hh*nlines*0.5 + math.random()*hh*2,
        z = endtextz,
        w = ww,
        h = hh * nlines,
        tx = screenSize.x - 25 - tw,
        yx = screenSize.x - 25 - yw,
        op = 1,
        op_spd = 1,
        life = 1,
        life_spd = 1,
        die_spd = 100,
    }
    table.insert(text.list, ltext)
    text.count = text.count + 1
end

function updateTextLabels(dt)
    -- text
    text.timer = text.timer - dt
    if text.timer <= 0 then
        text.delay = math.random(4)+4
        text.timer = text.delay
        addText()
    end
    for tti = #text.list,1,-1 do
        local tt = text.list[tti]
        if tt.life > 0 then
            tt.z = tt.z - dt * spd * text.spdmult
        else
            tt.z = tt.z - dt * spd * text.spdmult * 0.7
            tt.y = tt.y + dt * tt.die_spd
        end
        if tt.z <= 0 then
            if lazors.victim == tt then
                playhit(false)
            end
            table.remove(text.list, tti)
        elseif tt.life <= 0 then
            tt.op = tt.op - dt * tt.op_spd
            if tt.op <= 0 then
                table.remove(text.list, tti)
            end
        end
    end

    if text.count >= 5 and #text.list == 0 then
        text.count = 0
        warptime = warpdelay
    end
end

function drawTextLabels()
    if not titlesActive then
        return
    end

    local ww,hh = screenSize.x,screenSize.y*0.5
    -- text

    local used = false
    local t_min,t_max = 0.15,1.7

    for tti=#text.list,1,-1 do
        local tt = text.list[tti]
        if tt.z > 0 then
            local frac = math.pow(math.min(math.max(0, 1 - (tt.z-1)/endtextz),1), 2)

            -- quote itself
            local r,g,b = 59,182,193
            r = (r*tt.life + 255*(1-tt.life))
            g = (g*tt.life + 0*(1-tt.life))
            b = (b*tt.life + 0*(1-tt.life))

            love.graphics.setFont(labelsfont)
            love.graphics.push()
            love.graphics.translate(ww*0.5,horizon_height)
            love.graphics.scale(1/tt.z,1/tt.z)

            love.graphics.push()
            love.graphics.translate(2,2)
            love.graphics.setColor(0,0,0,255*tt.op)
            love.graphics.print(tt.txt, -tt.x, -tt.y)
            love.graphics.pop()

            love.graphics.setColor(r*frac,g*frac,b*frac, 255*tt.op)
            love.graphics.print(tt.txt, -tt.x, -tt.y)

            love.graphics.pop()


            -- title+year
            if tt.z <= t_max and tt.z >= t_min and not used then
                used = true
                local t_op = 1
                if tt.z >= t_max-0.5 then t_op = (t_max-tt.z)*2
                elseif tt.z <= t_min+0.5 then t_op = (tt.z-t_min)*2 end

                love.graphics.setFont(infofont)
                love.graphics.setColor(0,0,0,t_op * 255)

                love.graphics.print(tt.title, tt.tx, 27)
                love.graphics.print(tt.year, tt.yx, 52)

                love.graphics.setColor(255,255,255,t_op * 255)

                love.graphics.print(tt.title, tt.tx, 25)
                love.graphics.print(tt.year, tt.yx, 50)
            end
        end
    end
end


function switchTitles()
    titlesActive = not titlesActive
end