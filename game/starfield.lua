
local starzmin = 30
local star_floor = 16

local function genStar()
    local ww,hh = screenSize.x,screenSize.y*0.5
    return {
        x = (math.random() * ww - ww*0.5) * 5,
        y = (math.random()*hh)*5 + 200,
        z = math.random()*starzmin+starzmin
    }
end

function initStarField()


      ----- starfield
    stars = {}
    local nstars = screenSize.x*screenSize.y/800/600 * 300
    for i=1,nstars do
        stars[i] = genStar()
        stars[i].z = (stars[i].z - starzmin) * 2
    end
    taillength = 0.085

end


function updateStarField(dt)
    local ww,hh = screenSize.x,screenSize.y*0.5
    local tailcut = 0.1 - taillength*spd*2
    for i,star in ipairs(stars) do
        star.z = star.z - spd*dt
        if star.z < 0.1 - taillength*spd*2 then
            stars[i] = genStar()
        elseif  star.z > starzmin*2 then
            star.z = taillength*spd*2
            star.x = (math.random() * ww - ww*0.5) * 5
            star.y = (math.random()*hh)*5 + 200
        end
    end

end

function drawStarField()
    local ww,hh = screenSize.x,screenSize.y*0.5
    love.graphics.setLineWidth(1)
    love.graphics.setPointSize(1)

    for i,star in ipairs(stars) do
        local alp = math.min(10/star.z,1)
        alp = math.pow(alp,3.9)

        -- tail
        love.graphics.setColor(192,192,192,255*alp*0.2*star_opacity)
        local x,y = star.x/star.z + ww*0.5,horizon_height-star.y/star.z - star_floor
        if warp or forcewarp then
        -- if true then
            local zto = star.z + (spd-minspd) * taillength
            local xto,yto = star.x/zto + ww*0.5, horizon_height - star.y/zto - star_floor
            love.graphics.line(x,y,xto,yto)
        end

        -- head
        love.graphics.setColor(192,192,192,255*alp*star_opacity)
        love.graphics.point(x,y)
    end
end