
function love.load( )
    love.filesystem.load("colorspace.lua")()
    love.filesystem.load("animlogo.lua")()
    love.filesystem.load("quotes.lua")()
    love.filesystem.load("sounds.lua")()
    love.filesystem.load("starfield.lua")()
    love.filesystem.load("textlabels.lua")()
    love.filesystem.load("background.lua")()
    love.filesystem.load("lasers.lua")()
    love.filesystem.load("preload.lua")()

    initScreen()

    initSound()
    -- startSound()

    showPreload()

    initIcon()

end

function initIcon()
    local icon = love.graphics.newImage("img/ACEicon.png")
    love.window.setIcon(icon:getData())
    love.window.setTitle("ACE")
end

function initScreen()
    -- windowed
    screenSizeW = { x = 768/1080*1920, y = 768, fs = false }


    -- fullscreen
    local LL = love.window.getFullscreenModes()

    local supported = false
    local maxmode = LL[1]
    for _,mode in ipairs(LL) do
        if mode.width > maxmode.width then
            maxmode = mode
        end
        if mode.width == screenSizeW.width and mode.height == screenSizeW.height then
            supported = true
        end
    end

    if supported then
        screenSizeF = screenSizeW
    else
        local a1 = { x = maxmode.height/1080*1920, y = maxmode.height, fs = true }
        local a2 = { x = maxmode.width, y = maxmode.width*1080/1920, fs = true }
        if a1.x > maxmode.width then screenSizeF = a2 else screenSizeF = a1 end
    end

    screenSize = screenSizeW
    love.window.setMode(screenSize.x, screenSize.y, { fullscreen = false })

    fsCanvas = love.graphics.newCanvas(screenSizeW.x, screenSizeW.y)
end

function reset()
    dpth = 0
    minspd = 2
    maxspd = 12
    warptime = 0
    firstwarpdelay = 16
    secondwarpdelay = 12
    warpdelay = firstwarpdelay
    spd = minspd
    step = 0.5
    maxz = step*200

    horizon_height = screenSize.y * 0.6


    initBackground()

    initStarField()




    initLasers()
    -- maxlazor = 50

    generalTimer = 4
    generalPhase = 0
    -- generalPhase = 10

    initTextLabels()
    initLogo()

end



function love.update( dt )
    if preload then
        updatePreload(dt)
        return
    end

    -- warp
    if warptime > 0 then
        warp = true
        warptime = math.max(warptime - dt,0)
        if warptime > warpdelay*2/3 then
            local f = (warpdelay - warptime) / warpdelay * 3
            spd = minspd + (maxspd-minspd) * f
        elseif warptime < warpdelay/3 then
            local f = warptime/warpdelay*3
            spd = minspd + (maxspd-minspd) * f
        elseif warptime == 0 then
            warp = false
            warpdelay = secondwarpdelay
        end
    end


    -- road
    updateBackground(dt)
    updateStarField(dt)

    updateTextLabels(dt)

    updateLasers(dt)

    -- title
    generalTimer = generalTimer - dt
    if generalTimer <= 0 then
        generalTimer = 4
        generalPhase = generalPhase + 1

        -- warp
        if generalPhase == 4 then
            warptime = warpdelay
        end
    end

    updateLogo(dt)
    updateMousePointer(dt)
    updateSound(dt)
end

function love.draw( )
    preDraw()
    if preload then
        drawPreload()
        postDraw()
        return
    end

    drawBackground()
    drawStarField()
    drawTextLabels()
    drawLasers()

    -- drawLogo(screenSize.x*0.5, horizon_height*0.7)
    -- drawCredits(screenSize.x*0.5, horizon_height*0.7+80)
    drawLogoAnim()

    drawMousePointer()

    postDraw()
end

function preDraw()
    if fsActive then
        love.graphics.setCanvas(fsCanvas)
        love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill",0,0,screenSize.x,screenSize.y)
    end
end

function postDraw()
    if fsActive then
        love.graphics.setCanvas()
        love.graphics.push()
        love.graphics.scale(screenSizeF.x / screenSizeW.x, screenSizeF.y / screenSizeW.y)
        love.graphics.setColor(255,255,255)
        love.graphics.draw(fsCanvas)
        love.graphics.pop()

    end
end


function switchFullscreen()
    local fs = not love.window.getFullscreen( )
    if fs then
        love.window.setMode(screenSizeF.x, screenSizeF.y, { fullscreen = true })
        fsActive = screenSizeF.fs
    else
        love.window.setMode(screenSizeW.x, screenSizeW.y, { fullscreen = false })
        fsActive = screenSizeW.fs
    end

    redoLogoShader()
end

function love.keypressed( key )
    if key == "escape" then
        love.event.push("quit")
        return
    end

    if key == "f1" then
        switchFullscreen()
        return
    end

    if key == "f2" then
        switchTitles()
        return
    end

    if preload then
        finishPreload()
    end
end


function love.mousepressed(x,y,but)
    if preload then
        finishPreload()
        return
    end

    if not titlesActive then
        return
    end

    if forcewarp then
        local spdinc = 0.6
        if but == "wu" then
            spd = math.min(maxspd, spd + spdinc)
        elseif but == "wd" then
            spd = math.max(-maxspd, spd - spdinc)
        end
    end

    if but=="l" then
        shoot(getMouseCoords())
    end
end

function love.mousereleased(x,y,but)
    if preload then
        return
    end

    if but=="l" then
        unshoot()
    end
end
