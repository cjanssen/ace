secondquotes = {
{
    {
    "The breakfast club",
    "1985",
    },
    {
    "Does Barry Manilow know that you raid his wardrobe?",
    }
},

{
    {
    "Back to the future",
    "1985",
    },
    {
    "The way I see it, if you're gonna build a time machine\ninto a car, why not do it with some style?",
    "Roads? Where we're going we don't need... roads!",
    "In 1985 plutonium is in every corner drug store,\nbut in 1955, its a little hard to come by!",
    "I guess you guys aren't ready for that yet.\nBut your kids are gonna love it. ",
    }
},

{
    {
    "Ferris Bueller's day off",
    "1986",
    },
    {
    "Life moves pretty fast. If you don't stop\nand look around once in a while,\nyou could miss it.",
    "Pardon my French, but you're an asshole, asshole!",
    }
},

{
    {
    "Ghostbusters",
    "1984",
    },
    {
    "Don't cross the streams!",
    "We came, we saw, we kicked its ass!",
    "I've quit better jobs than this.",
    "He slimed me.",
    }
},

{
    {
    "Star Wars: the empire strikes back",
    "1980",
    },
    {
    "* I love you \n* I know",
    "Do... or do not. There is no try.",
    "No, I am your father.",
    }
},

{
    {
    "The Goonies",
    "1985",
    },
    {
    "I'm gonna hit you so hard that when you wake up\nyour clothes will be out of style!",
    }
},

{
    {
    "Indiana Jones and the Raiders of the Lost Ark",
    "1981",
    },
    {
    "Snakes. Why'd it have to be snakes?",
    "Don't look at it. Shut your eyes, Marion.",
    "Throw me idol, I'll throw you the whip!",
    }
},

{
    {
    "Star wars: Return of the Jedi",
    "1983",
    },
    {
    "Your thoughts betray you, Father.\nI feel the good in you, the conflict.",
    "That name no longer has any meaning for me!",
    "It's a trap!",
    }
},

{
    {
    "Top gun",
    "1986",
    },
    {
    "I feel the need... the need for speed!",
    "Negative, Ghost Rider. The pattern is full",
    }
},

{
    {
    "The princess bride",
    "1987",
    },
    {
    "Hello. My name is Inigo Montoya.\nYou killed my father.\nPrepare to die.",
    "You keep using that word. I do not think\nit means what you think it means.",
    }
},

{
    {
    "Blade Runner",
    "1982",
    },
    {
    "All those moments will be lost in time\nlike tears in rain",
    "It's too bad she won't live!\nBut then again, who does?",
    "Quite an experience to live in fear, isn't it?\nThat's what it is to be a slave.",
    "'More human than human' is our motto. ",
    "Let me tell you about my mother.",
    }
},

{
    {
    "Karate kid",
    "1984",
    },
    {
    "Wax on, wax off. Wax on, wax off.",
    "Man who catch fly with chopstick accomplish anything.",
    }
},

{
    {
    "Aliens",
    "1986",
    },
    {
    "Get away from her, you bitch!",
    "* Have you ever been mistaken for a man?\n* No, have you?",
    "LET'S ROCK!",
    "'cause it'll be dark soon,\nand they mostly come at night...mostly.",
    }
},

{
    {
    "Crocodile Dundee",
    "1986",
    },
    {
    "That's not a knife.  THAT's a knife.",
    }
},

{
    {
    "Terminator",
    "1984",
    },
    {
    "Come with me if you want to live.",
    "I'll be back!",
    "You're terminated, fucker.",
    }
},

{
    {
    "Indiana Jones and the temple of doom",
    "1984",
    },
    {
    "Ah, dessert! Chilled monkey brains.",
    }
},

{
    {
    "Poltergeist",
    "1982",
    },
    {
    "They're here.",
    }
},

{
    {
    "Labyrinth",
    "1986",
    },
    {
    "Everything I've done, I've done for you.\nI move the stars for no one.",
    "For my will is as strong as yours,\nand my kingdom is as great,\nyou have no power over me.",
    }
},

{
    {
    "The shining",
    "1980",
    },
    {
    "All work and no play makes Jack a dull boy.",
    }
},

{
    {
    "Who framed roger rabbit",
    "1988",
    },
    {
    "I'm not bad. I'm just drawn that way.",
    "Traffic jams will be a thing of the past.",
    "Is that a rabbit in your pocket or are you just happy to see me?",
    }
},

{
    {
    "Batman",
    "1989",
    },
    {
    "You ever dance with the devil in the pale moonlight?",
    "Where does he get those wonderful toys?",
    }
},

{
    {
    "Die Hard",
    "1988",
    },
    {
    "Now I have a machine gun. Ho ho ho.",
    "Yippee*ki*yay, motherfucker!",
    "Who's driving this car, Stevie Wonder?",
    }
},

{
    {
    "Rocky 3",
    "1982",
    },
    {
    "I pity the fool!",
    "My prediction? Pain!",
    }
},

{
    {
    "Big trouble in Little China",
    "1986",
    },
    {
    "It's all in the reflexes.",
    }
},

{
    {
    "Rocky 4",
    "1985",
    },
    {
    "I must break you.",
    }
},

{
    {
    "Mad Max Beyond Thunderdome",
    "1985",
    },
    {
    "Two men enter, one man leave! ",
    "PLAN? There ain't no plan!",
    }
},

{
    {
    "WarGames",
    "1983",
    },
    {
    "A strange game. The only winning move is not to play.\nHow about a nice game of chess? ",
    "Shall we play a game?",
    "How about Global Thermonuclear War?",
    }
},

{
    {
    "Star trek 4 : the voyage home",
    "1986",
    },
    {
    "Computer! Computer? Hello, computer!",
    }
},

{
    {
    "Platoon",
    "1986",
    },
    {
    "I think now, looking back,\nwe did not fight the enemy;\nwe fought ourselves.",
    "Excuses are like assholes, Taylor, everybody got one.",
    }
},

{
    {
    "Indiana jones and the last crusade",
    "1989",
    },
    {
    "I've got a lot of fond memories of that dog.",
    "X never, ever marks the spot.",
    "He chose poorly.",
    "Let my armies be the rocks and the trees and the birds in the sky",
    }
},

{
    {
    "When harry met sally ",
    "1989",
    },
    {
    "I'll have what she's having.",
    }
},

{
    {
    "Superman 2",
    "1980",
    },
    {
    "Come to me, son of Jor*El, kneel before Zod!",
    }
},

{
    {
    "Ghostbusters 2",
    "1989",
    },
    {
    "Death is but a door. Time is but a window.",
    }
},

{
    {
    "Roxanne",
    "1987",
    },
    {
    "is that your nose or did a bus park on your face?",
    "when you stop to smell the flowers, are they afraid?",
    }
},

{
    {
    "A fish called wanda",
    "1988",
    },
    {
    "To call you stupid would be an insult to stupid people!",
    "The central message of Buddhism is not 'every man for himself'.",
    }
},

{
    {
    "Mad Max 2: the road warrior",
    "1981",
    },
    {
    "I'm just here for the gasoline.",
    "Be still my dog of war.",
    "The gasoline will be ours. Then you shall have your revenge.",
    "Again you have made me unleash my dogs of war.",
    }
},

{
    {
    "The neverending story",
    "1984",
    },
    {
    "Why don't you do what you dream, Bastian?",
    "Fantasia has no boundaries.",
    "We don't even care whether or not we care.",
    }
},

{
    {
    "The fly",
    "1986",
    },
    {
    "Be afraid.  Be very afraid.",
    }
},

{
    {
    "Beverly Hills cop",
    "1984",
    },
    {
    "This thing's nicer than my apartment.",
    "You're not going to fall for the banana in the tailpipe.",
    }
},

{
    {
    "First blood",
    "1982",
    },
    {
    "I didn't come to rescue Rambo from you.\nI came here to rescue you from him.",
    "A man who's been trained to ignore pain,\nignore weather, to live off the land,\nto eat things that would make a billy goat puke.",
    "With what? I can't find your fuckin' legs!",
    }
},

{
    {
    "Masters of the Universe",
    "1987",
    },
    {
    "I have the power! ",
    }
},

{
    {
    "The Thing",
    "1982",
    },
    {
    "I dunno what the hell's in there,\nbut it's weird and pissed off,\nwhatever it is.",
    }
},

{
    {
    "Predator",
    "1987",
    },
    {
    "You're one ugly motherfucker!",
    "I ain't got time to bleed.",
    "If it bleeds, we can kill it.",
    }
},

{
    {
    "Dead Poets Society",
    "1989",
    },
    {
    "Carpe diem, seize the day boys,\nmake your lives extraordinary.",
    "I'm exercising the right not to walk.",
    }
},

{
    {
    "Scarface",
    "1983",
    },
    {
    "Say hello to my little friend!",
    "All I have in this world is my balls and my word\nand I don't break them for no one.",
    }
},

{
    {
    "Airplane",
    "1980",
    },
    {
    "I am serious... and don't call me Shirley.",
    "Looks like I picked the wrong week to quit sniffing glue.",
    "We have clearance, Clarence.\nRoger, Roger.\nWhat's our vector, Victor?",
    "You ever seen a grown man naked?",
    }
},

{
    {
    "Lethal Weapon",
    "1987",
    },
    {
    "I'm too old for this shit.",
    "I'm not a cop tonight, Rog. This is personal.",
    "* Diplomatic immunity!\n* It's just been revoked!",
    }
},

{
    {
    "Sudden Impact",
    "1983",
    },
    {
    "Go ahead, make my day.",
    }
},

{
    {
    "Full Metal Jacket",
    "1987",
    },
    {
    "I bet you're the kind of guy that would fuck a person in the ass\nand not even have the goddamn common courtesy\nto give him a reach*around.",
    "Five foot nine, I didn't know they stacked shit that high!",
    "Texas? Only steers and queers come from Texas, Private Cowboy,\nand you don't look much like a steer to me.",
    "You talk the talk. Do you walk the walk? ",
    }
},

{
    {
    "Highlander",
    "1986",
    },
    {
    "There can be only one!",
    }
},

{
    {
    "They Live",
    "1988",
    },
    {
    "I have come here to chew bubblegum and kick ass...\nand I'm all out of bubblegum.",
    "We are their cattle.\nWe are being bred for slavery.",
    }
},

{
    {
    "Wall Street",
    "1987",
    },
    {
    "If you need a friend, get a dog.",
    "greed, for lack of a better word, is good.",
    "Life all comes down to a few moments.\nThis is one of them.",
    }
},

{
    {
    "Blues Brothers",
    "1980",
    },
    {
    "It's 106 miles to Chicago, we got a full tank of gas,\nhalf a pack of cigarettes, it's dark...\nand we're wearing sunglasses.",
    "We're on a mission from God.",
    "We got country *and* western.",
    "Our Lady of Blessed Acceleration, don't fail me now.",
    }
},

{
    {
    "Dune",
    "1984",
    },
    {
    "I must not fear. Fear is the mind*killer.",
    "God created Arakis to train the faithful.",
    "He who controls the Spice, controls the universe!",
    "The Spice must flow.",
    }
},

{
    {
    "Tootsie",
    "1982",
    },
    {
    "That is one nutty hospital.",
    }
},

{
    {
    "Trading Places",
    "1983",
    },
    {
    "Say, man, when I was growin' up,\nwe wanted a Jacuzzi, we had to fart in the tub.",
    }
},

{
    {
    "RoboCop",
    "1987",
    },
    {
    "Dead or alive, you're coming with me!",
    }
},
{
    {
    "Conan the barbarian",
    "1982",
    },
    {
    "Conan! What is best in life?",
    "Crush your enemies.\nSee them driven before you.\nHear the lamentations of their women.",
    "Grant me revenge! And if you do not listen, then to HELL with you!",
    "Now they will know why they are afraid of the dark.",
    "Let me tell you of the days of high adventure!",
    "They shall all drown in lakes of blood.",
    },
},
{
    {
    "Conan the destroyer",
    "1984",
    },
    {
    "A fine magician you are! Go back to juggling apples.",
    "My brother's sister's cousin never said anything about bars.",
    },
},
{
    {
    "Willow",
    "1988",
    },
    {
    "Ooh, I'm really scared. No! Don't!\nThere's a peck here with an acorn pointed at me!",
    "* Wanna breed?\n* Tempting... but no.",
    "Look at her! I could use a love potion on her!",
    "Forget all you know, or think you know.",
    "Well my mother raised me on blackroot. It's good for you.\nIt puts hair on your chest.",
    "I am the greatest swordsman that ever lived.\nSay, um, can I have some of that water?",
    },
},

}
