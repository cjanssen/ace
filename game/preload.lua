function showPreload()
    preload = true
    -- reset()
    preloadfont = love.graphics.newFont("fnt/LiquidCrystal-Normal.otf",24)
    titlesActive = true
end

function finishPreload()
    startSound()
    reset()
    preload = false
end

function updatePreload(dt)
end

function drawPreload()
    local fs = love.window.getFullscreen()
    love.graphics.setFont(preloadfont)
    love.graphics.setColor(255,255,255)
    love.graphics.print("F1 :       Switch fullscreen ("..(fs and "ON" or "OFF")..")", 450, 250)
    love.graphics.print("F2 :       Switch quotes ("..(titlesActive and "ON" or "OFF")..")", 450, 290)
    love.graphics.print("ESC :     Exit", 450, 330)
    love.graphics.print("Press any key or click to continue", 450, 450)

end