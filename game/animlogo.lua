local logo = {}

function initLogo()
    -- logo animation
    logo = {
        front = love.graphics.newImage("img/frontlayer.png"),
        back = love.graphics.newImage("img/backlayer.png"),
        angle = 0,
        rotspd = 2,
    }

    local shadercode = [[
        uniform Image im2;
        uniform vec2 im2pos;
        uniform vec2 im2size;
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            vec4 pixel = Texel(texture, texture_coords );
            screen_coords.y = love_ScreenSize.y - screen_coords.y;
            vec2 dcoord = (screen_coords - im2pos) / im2size;
            vec4 pix2 = Texel(im2, dcoord);
            return color*pixel*pix2;
        }
    ]]

    logo.shader = love.graphics.newShader(shadercode)
    logo.shader:send("im2",logo.back)
    logo.shader:send("im2size",{logo.back:getWidth(), logo.back:getHeight()})

    local ww,hh = screenSize.x,screenSize.y
    divs = 12
    local wi = ww/divs

    local vertices = {}
    for i=0,divs do
        local c = 255 - ((i+1)%2)*148
        table.insert(vertices, {
            -ww*0.5 + wi*i, -hh*0.5,
            i/divs,0,
            c,c,c,
            })
        table.insert(vertices, {
            -ww*0.5 + wi*i, hh*0.5,
            i/divs,1,
            c,c,c,
            })
    end

    local oldCanvas = love.graphics.getCanvas()

    logo.canvas = love.graphics.newCanvas(logo.back:getWidth(),logo.back:getHeight())

    local canvas = love.graphics.newCanvas(2,2)
    love.graphics.setCanvas(canvas)
    love.graphics.setColor(255,255,255)
    love.graphics.rectangle("fill",0,0,2,2)
    love.graphics.setCanvas(oldCanvas)
    local whiteimg = love.graphics.newImage(canvas:getImageData())

    logo.gradient = love.graphics.newMesh(vertices, whiteimg, "strip")


    -- credits graphics
    local creditsSmall = love.graphics.newFont("fnt/LiquidCrystal-Normal.otf",16)
    local creditsBig = love.graphics.newFont("fnt/LiquidCrystal-Normal.otf",21)

    local canvas = love.graphics.newCanvas(320,50)
    love.graphics.setCanvas(canvas)
    local function printSpc(txt, x, y, font)
        love.graphics.setFont(font)
        local w = font:getWidth(txt)
        love.graphics.print(txt,x,y)
        return x+w
    end

    local hplus = creditsBig:getHeight() - creditsSmall:getHeight() - 2
    for i=1,0,-1 do
        if i==1 then
            love.graphics.setColor(0,0,0)
        else
            love.graphics.setColor(255,255,255)
        end
        local w = 2
        w = printSpc("by",w,i+hplus, creditsSmall)
        w = printSpc(" Norman Ritter ",w,i, creditsBig)
        logo.creditsWLeft = w
    end

    logo.creditsLeft = love.graphics.newImage(canvas:getImageData())
    local canvas = love.graphics.newCanvas(320,50)
    love.graphics.setCanvas(canvas)
    for i=1,0,-1 do
        local w = 2
        if i==1 then
            love.graphics.setColor(0,0,0)
        else
            love.graphics.setColor(255,255,255)
        end
        w = printSpc("and",w,i+hplus, creditsSmall)
        w = printSpc(" Christiaan Janssen",w,i, creditsBig)
        logo.creditsWRight = w
    end

    logo.creditsWidth = logo.creditsWLeft + logo.creditsWRight
    love.graphics.setCanvas(oldCanvas)
    logo.creditsRight = love.graphics.newImage(canvas:getImageData())

    -- positions
    resetLogoAnim()
end

function redoLogoShader()
    if logo and logo.shader then
        logo.shader:send("im2",logo.back)
        logo.shader:send("im2size",{logo.back:getWidth(), logo.back:getHeight()})
    end
end

function resetLogoAnim()
    local cw,ch = screenSize.x*0.5, horizon_height*0.7
    logo.posanim = {
        logoinit = {x = cw, y = -logo.back:getHeight(), z = 1},
        logodest = {x = cw, y = ch, z = 1},
        leftinit = {x = -logo.creditsWLeft, y = ch + 80, z = 1},
        leftdest = {x = cw - logo.creditsWidth * 0.5, y = ch + 80, z = 1},
        rightinit = {x = cw*2, y = ch + 80, z = 1},
        rightdest = {x = cw - logo.creditsWidth * 0.5+ logo.creditsWLeft, y = ch + 80, z = 1},

        phase = 1,
        timer = 0,
        delays = {4,1,6,4,10},
        spd = 0.65,
    }
end

function updateLogo(dt)
    if logo.posanim.phase <= #logo.posanim.delays then
        -- shading angle
        logo.angle = (logo.angle + logo.rotspd * dt) % (math.pi*2)

        -- general timer
        logo.posanim.timer = logo.posanim.timer + dt
        if logo.posanim.timer > logo.posanim.delays[logo.posanim.phase] then
            logo.posanim.timer = logo.posanim.timer - logo.posanim.delays[logo.posanim.phase]
            logo.posanim.phase = logo.posanim.phase + 1

            -- special case for this switch
            if logo.posanim.phase == 3 then
                logo.posanim.logopos = logo.posanim.logodest
                logo.posanim.leftpos = logo.posanim.leftdest
                logo.posanim.rightpos = logo.posanim.rightdest
            end
        end

        local function mixVec(v1,v2,f)
            return {
                x = v2.x * f + v1.x * (1-f),
                y = v2.y * f + v1.y * (1-f),
                z = v2.z * f + v1.z * (1-f),
            }
        end
        -- proper anim
        if logo.posanim.phase == 2 then
            local frac = logo.posanim.timer / logo.posanim.delays[logo.posanim.phase]
            logo.posanim.logopos = mixVec(logo.posanim.logoinit, logo.posanim.logodest, frac)
            logo.posanim.leftpos = mixVec(logo.posanim.leftinit, logo.posanim.leftdest, frac)
            logo.posanim.rightpos = mixVec(logo.posanim.rightinit, logo.posanim.rightdest, frac)
        elseif logo.posanim.phase == 4 then
            logo.posanim.logopos.z = logo.posanim.logopos.z - logo.posanim.spd * dt
            logo.posanim.leftpos.z = logo.posanim.leftpos.z - logo.posanim.spd * dt
            logo.posanim.rightpos.z = logo.posanim.rightpos.z - logo.posanim.spd * dt
        end
    end
end

function prepareLogoDraw()
    local oldCanvas = love.graphics.getCanvas()
    logo.canvas:clear()
    love.graphics.setCanvas(logo.canvas)


    local w2,h2 = logo.front:getWidth()*0.5, logo.front:getHeight()*0.5
    love.graphics.setColor(255,255,255)
    logo.shader:send("im2pos",{0,0})

    love.graphics.setShader(logo.shader)
    love.graphics.push()
    love.graphics.translate(w2,h2)

    love.graphics.rotate(logo.angle)
    love.graphics.draw(logo.gradient)
    love.graphics.pop()
    love.graphics.setShader()


    love.graphics.setColor(255,255,255)
    love.graphics.draw(logo.front, 0, 0)

    love.graphics.setCanvas(oldCanvas)

    -- love.graphics.draw(logo.canvas, math.floor(x-w2), math.floor(y-h2))
end

function drawCredits(x,y)

    love.graphics.setColor(255,255,255)
    love.graphics.draw(logo.creditsLeft, math.floor(x - logo.creditsWidth*0.5),math.floor(y))
    love.graphics.draw(logo.creditsRight, math.floor(x - logo.creditsWidth*0.5 + logo.creditsWLeft),math.floor(y))
end

function drawLogoAnim()
    if logo.posanim.phase < 2 or logo.posanim.phase > 4 or logo.posanim.logopos.z < 0.01 then return end

    local ww, hh = screenSize.x*0.5, horizon_height
    local w2,h2 = logo.front:getWidth()*0.5, logo.front:getHeight()*0.5

    love.graphics.setColor(255,255,255)
    prepareLogoDraw()

    love.graphics.push()
    love.graphics.translate(ww,hh)
    love.graphics.scale(1/logo.posanim.logopos.z, 1/logo.posanim.logopos.z)

    -- logo
    -- drawLogo(logo.posanim.logopos.x - ww, logo.posanim.logopos.y - hh)
    love.graphics.draw(logo.canvas, logo.posanim.logopos.x - ww - w2, logo.posanim.logopos.y - hh - h2)


    -- left
    love.graphics.draw(logo.creditsLeft,
        math.floor(logo.posanim.leftpos.x - ww),
        math.floor(logo.posanim.leftpos.y - hh))


    -- right
    love.graphics.draw(logo.creditsRight,
        math.floor(logo.posanim.rightpos.x - ww),
        math.floor(logo.posanim.rightpos.y - hh))

    love.graphics.pop()

end
