
local function createMts()
    math.randomseed(1)
    local fracs = {0,1}
    for i=3,150 do fracs[i] = math.random() end
    table.sort(fracs)

    mountns = {}
    for i=1,#fracs do mountns[i] = {fracs[i], math.random()} end


    -- local ww,hh = screenSize.x,screenSize.y*0.5 + 20
    -- mntpoly = {0,hh}
    -- for i=1,#mountns do
    --     mntpoly[i*2+1] = ww * mountns[i][1]
    --     mntpoly[i*2+2] = hh - 10 - 70*mountns[i][2]
    -- end
    -- table.insert(mntpoly,ww)
    -- table.insert(mntpoly,hh)

    local ww = screenSize.x
    local x0 = ww*0.25
    local oldCanvas = love.graphics.getCanvas()
    local canvas = love.graphics.newCanvas(screenSize.x, screenSize.y)
    love.graphics.setCanvas(canvas)
    love.graphics.setColor(0,0,0)

    for i=1,#mountns do
        local xx,hh = mountns[i][1]*(ww-x0*2) + x0, (mountns[i][2]+0.1) * 33
        -- local amp = math.sin(mountns[i][1] * math.pi)
        amp = 1-math.abs(1-mountns[i][1]*2)
        hh = hh * amp
        if hh > 5 then
            love.graphics.polygon("fill", xx, horizon_height - hh, xx+hh, horizon_height, xx-hh, horizon_height)
        end
    end

    love.graphics.setCanvas(oldCanvas)
    mntImg = love.graphics.newImage(canvas:getImageData())
    math.randomseed(love.timer.getTime())

end



local function prepareFlyers()
    local oo = math.random(2)
    local fh = (math.random() - 0.5) * 40
    local r,g,b = hsv2rgb(math.random(360),255,255)
    flyer = {
        delay = 5 + math.random(5),
        timer = 0,
        x = oo==1 and 0 or screenSize.x,
        spd = (math.abs(fh)+5)*200 * (oo==1 and 1 or -1),
        y = math.floor(fh + horizon_height),
        color = {r = r, g = g, b = b},
    }
    flyer.timer = flyer.delay

end

local function prepareComets()
    local ww,hh = screenSize.x, screenSize.y
    local ang = (math.random()-0.5) * math.pi * 0.5 + math.pi * 0.5
    comet = {
        x = (math.random() * 0.5 + 0.25) * ww,
        y = (math.random() - 0.5) * hh,
        op = 0,
        dir = { x = math.cos(ang), y = math.sin(ang) },
        spd = 400 + 100 * math.random(),
        timer = 0,
        delay = math.random()*20 + 5,
        frac = 0,
        op_spd = 2,
        phase = 0,
    }

end

local function prepareSkyCycle()
    skyCycle = {
    {
        {0,0,255,0},
        {255,0,128,128},
        {255,192,32,255},
        sop = 1,
    },
    {
        {0,0,255,0},
        {255,0,128,128},
        {255,192,32,255},
        sop = 1,
    },
    {
        {0,0,255,0},
        {255,0,32,64},
        {255,64,16,128},
        sop = 1,
    },
    {
        {0,0,255,0},
        {32,0,128,64},
        {64,0,0,128},
        sop = 1,
    },
    {
        {0,0,255,0},
        {0,0,255,16},
        {0,0,255,32},
        sop = 1,
    },
    {
        {0,0,0,0},
        {0,0,0,16},
        {0,0,128,24},
        sop = 1,
    },
    {
        {0,0,0,0},
        {0,32,128,24},
        {128,32,0,32},
        sop = 1,
    },
    {
        {0,0,64,0},
        {0,128,164,32},
        {128,64,0,64},
        sop = 0.5,
    },
    {
        {0,128,192,16},
        {0,192,255,64},
        {140,100,0,192},
        sop = 0.25,
    },
    {
        {0,128,192,64},
        {0,192,255,128},
        {32,100,64,255},
        sop = 0.05,
    },
    {
        {0,164,220,255},
        {16,64,255,255},
        {32,32,255,255},
        sop = 0,
    },
    {
        {0,128,255,255},
        {0,164,255,255},
        {0,164,255,255},
        sop = 0,
    },
    {
        {0,220,255,192},
        {0,168,255,255},
        {192,128,128,255},
        sop = 0.8,
    },
    {
        {0,220,255,32},
        {220,16,128,192},
        {192,92,32,255},
        sop = 1,
    }
    }

    skyCycle.predelay = 125
    skyCycle.ndx = 1
    skyCycle.timer = 0
    skyCycle.delay = 65
    star_opacity = 1
end

local function createSkyMesh()
    local oldCanvas = love.graphics.getCanvas()
    local canvas = love.graphics.newCanvas(2,2)
    love.graphics.setCanvas(canvas)
    love.graphics.setColor(255,255,255)
    love.graphics.rectangle("fill",0,0,2,2)
    love.graphics.setCanvas(oldCanvas)
    local img = love.graphics.newImage(canvas:getImageData())

    local ww,hh = screenSize.x, screenSize.y*0.5 + 20
    local vertxs = {
        {
            0, 0, -- position of the vertex
            0, 0, -- texture coordinate at the vertex position
            0, 0, 255, 0, -- color of the vertex
        },
        {
            ww, 0,
            1, 0, -- texture coordinates are in the range of [0, 1]
            0, 0, 255, 0,
        },
        {
            -- bottom-left corner (yellow-tinted)
            0, horizon_height*0.75,
            0, 1,
            255, 0, 128, 128,
        },
        {
            -- bottom-right corner (blue-tinted)
            ww,horizon_height*0.75,
            1, 1,
            255, 0, 128, 128,
        },
        {
            -- bottom-left corner (yellow-tinted)
            0, horizon_height,
            0, 1,
            255, 192, 32
        },
        {
            -- bottom-right corner (blue-tinted)
            ww,horizon_height,
            1, 1,
            255, 192, 32
        },
    }

    -- the Mesh DrawMode "fan" works well for 4-vertex Meshes.
    skymesh = love.graphics.newMesh(vertxs, img, "strip")
    skycolors = {
        {0,0,255,0},
        {255,0,128,128},
        {255,192,32,255}
    }
    skymesh_vertxs = vertxs
    prepareSkyCycle()
end


function initBackground()
    ------ color
    colndx = 2
    coltim = 0
    colspd = 0.01
    coldn = 64 --96
    colup = 255 --192

    warp = false
    forcewarp = false

    -- bg
    createSkyMesh()
    createMts()
    prepareFlyers()
    prepareComets()

    flyer.x = flyer.x + flyer.spd * screenSize.x
    comet.phase = 3
    comet.delay = warpdelay*2
end

local function updateSkyCycle(dt)
    if skyCycle.predelay > 0 then
        skyCycle.predelay = skyCycle.predelay - dt
        return
    end

    skyCycle.timer = skyCycle.timer + dt
    if skyCycle.timer > skyCycle.delay then
        skyCycle.timer = skyCycle.timer - skyCycle.delay
        skyCycle.ndx = (skyCycle.ndx % #skyCycle) + 1
    end

    local frac = skyCycle.timer / skyCycle.delay
    local nundx = (skyCycle.ndx % #skyCycle) + 1
    for i,col in ipairs(skycolors) do
        for j,component in ipairs(col) do
            skycolors[i][j] = skyCycle[skyCycle.ndx][i][j] * (1-frac) + skyCycle[nundx][i][j] * frac
        end
    end

    star_opacity = skyCycle[skyCycle.ndx].sop * (1-frac) + skyCycle[nundx].sop * frac

    for i,v in ipairs(skymesh_vertxs) do

        local ndx = math.floor((i-1)/2)+1
        skymesh:setVertex(i, {v[1],v[2],v[3],v[4],
            skycolors[ndx][1],skycolors[ndx][2],skycolors[ndx][3],skycolors[ndx][4]})

    end
end

function updateBackground(dt)
    dpth = (dpth + spd * dt) % step
    selcol = { 0, 38, 232 }

    -- flyer
    flyer.timer = flyer.timer - dt
    if flyer.timer <= 0 and (flyer.x < 0 or flyer.x > screenSize.x)  then
        prepareFlyers()
    end
    flyer.x = flyer.x + flyer.spd * dt

    -- comet
    if spd == minspd then
        comet.timer = comet.timer + dt
        if comet.timer >= comet.delay then
            prepareComets()
        end
    end

    if comet.phase < 2 then
        comet.frac = comet.frac + dt * comet.op_spd * (comet.phase == 0 and 1 or -1)
        if comet.phase == 0 and comet.frac >= 1 then
            comet.frac = 1
            comet.phase = 1
        elseif comet.phase == 1 and comet.frac <= 0 then
            comet.frac = 0
            comet.phase = 2
            comet.op = 0
        end


        comet.x = comet.x + comet.spd * comet.dir.x * dt
        comet.y = comet.y + comet.spd * comet.dir.y * dt

        local yfrac = math.max(0, (horizon_height - comet.y)/horizon_height)
        local wfrac = math.pow(1 - (spd-minspd)/maxspd, 6)
        comet.op = math.pow(comet.frac, 2) * yfrac * wfrac
    end

    -- sky
    updateSkyCycle(dt)
end

local function drawPiramids()
    local ww = screenSize.x
    local x0 = ww*0.25
    love.graphics.setColor(0,0,0,192)
    for i=1,#mountns do
        local xx,hh = mountns[i][1]*(ww-x0*2) + x0, (mountns[i][2]+0.1) * 30
        -- local amp = math.sin(mountns[i][1] * math.pi)
        amp = 1-math.abs(1-mountns[i][1]*2)
        hh = hh * amp
        if hh > 5 then
            love.graphics.polygon("fill", xx, horizon_height - hh, xx+hh, horizon_height, xx-hh, horizon_height)
        end
    end
end

function drawBackground()
    alp = 1
    love.graphics.setLineWidth(1)
    love.graphics.setPointSize(1)
    love.graphics.setColor(selcol[1]*alp,selcol[2]*alp,selcol[3]*alp)
    local ww,hh = screenSize.x,screenSize.y
    love.graphics.setScissor(0,horizon_height, ww,hh)
    for xx = -ww*10,ww*11,ww/2.5 do
        love.graphics.line(ww*0.5,horizon_height,xx,hh)
    end

    local barhh = horizon_height - 20
    for zz = 1,maxz,step do
        local z = (zz - dpth) * 1.5
        if z > 0 then
            local yy = barhh/z + barhh
            if yy<=hh then
                local op = 1 -  (yy - barhh)/barhh
                local alp = 0.5 + 0.5 * math.pow(op,4)

                love.graphics.setColor(selcol[1]*alp,selcol[2]*alp,selcol[3]*alp)
                love.graphics.line(0,yy,ww,yy)
                if yy - barhh <= 20 then
                    break
                end
            end
        end
    end
    love.graphics.setScissor()

    -- background
    love.graphics.draw(skymesh)

    -- comet
    if comet.phase <= 2 and comet.op > 0 then
        love.graphics.setLineWidth(1)
        love.graphics.setScissor(0,0,ww,horizon_height)
        local comet_tail_fac = 0.01
        local vx,vy = -comet.dir.x*comet.spd*comet_tail_fac,-comet.dir.y*comet.spd*comet_tail_fac
        love.graphics.setLineWidth(1)
        love.graphics.setColor(255,255,255,255 * comet.op * star_opacity)
        love.graphics.line(comet.x, comet.y, comet.x + vx, comet.y+vy)
        love.graphics.setColor(255,255,255,255 * comet.op * 0.2 * star_opacity)
        love.graphics.line(comet.x+vx, comet.y+vy, comet.x+vx*5, comet.y+vy*5)
        love.graphics.setScissor()
    end


    -- mountains
    drawPiramids()


    -- flyer
    love.graphics.setColor(flyer.color.r, flyer.color.g, flyer.color.b)
    love.graphics.setLineWidth(1)
    love.graphics.line(flyer.x, flyer.y, flyer.x - flyer.spd * 0.04, flyer.y)
end