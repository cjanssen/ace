function initLasers()
    lazors = {
        hit = false,
        timer = 0,
        spd = 1,

    }

    -- mouse
    mouseAngle = 0
    mouseSpd = 1
    mouseColor = {0,255,0}

end

function updateLasers(dt)
        -- lazors
    if lazors.hit and lazors.victim.z<0.2 then playhit(false) end
    lazors.timer = (lazors.timer + dt * lazors.spd)%1
    if lazors.hit then
        lazors.victim.life = lazors.victim.life - dt * lazors.victim.life_spd
        if lazors.victim.life <= 0 then
            lazors.victim.life = 0
            playhit(false)
        end
    end
end


function shoot(x,y)
    local target = evalTarget(x,y)
    if target then
        playhit(true)
        lazors.victim = target
    end
end

function evalTarget(x,y)
    local ww = screenSize.x
    for _,tt in ipairs(text.list) do
        local x0,y0 = -tt.x/tt.z+ww*0.5, -tt.y/tt.z+horizon_height
        local x1,y1 = (-tt.x+tt.w)/tt.z+ww*0.5, (-tt.y+tt.h)/tt.z+horizon_height

        if x>=x0 and x<=x1 and y>=y0 and y<=y1 then
            return tt
            -- break
        end
    end
    return nil
end

function playhit(hit)
    lazors.hit = hit
    playLaserSound(hit)
end

function unshoot()
    playhit(false)
end

function getMouseCoords()
    local mx,my = love.mouse.getX(),love.mouse.getY()
    if fsActive then
        mx = mx * screenSizeW.x/screenSizeF.x
        my = my * screenSizeW.y/screenSizeF.y
    end
    return mx,my
end

function drawLasers()
    -- lazors
    local sw,sh = screenSize.x, screenSize.y
    if lazors.hit then
        local brd = 0.01 + math.sin(lazors.timer*math.pi*2)*0.005
        love.graphics.setColor(255,0,0,128)
        local xc,yc = getMouseCoords()
        love.graphics.polygon("fill",
            sw*(0.5-brd),sh,
            xc - sw*brd*0.15/lazors.victim.z, yc,
            xc + sw*brd*0.15/lazors.victim.z, yc,
            sw*(0.5+brd),sh
            )


        brd = brd * 0.75
        love.graphics.setColor(255,0,0)
        -- local xc = (-lazors.victim.x+lazors.victim.w*0.5)/lazors.victim.z + sw*0.5
        -- local yc = (-lazors.victim.y+lazors.victim.h*0.5)/lazors.victim.z + horizon_height
        love.graphics.polygon("fill",
            sw*(0.5-brd),sh,
            xc - sw*brd*0.15/lazors.victim.z, yc,
            xc + sw*brd*0.15/lazors.victim.z, yc,
            sw*(0.5+brd),sh
            )

        -- love.graphics.setColor(255,0,0)
        -- love.graphics.line(sw*0.5,sh,xc,yc)
    end
end


function updateMousePointer(dt)
    -- get position
    local mx, my = getMouseCoords()
    local wasTargeted = mouseTargeted
    mouseTargeted = evalTarget(mx, my) ~= nil

    if wasTargeted and not mouseTargeted then playhit(false) end

    -- color
    if lazors and lazors.hit then
        mouseColor = {255,0,0,128}
        mouseSpd = -4.5
    else
        if mouseTargeted then
            mouseColor = {192,188,16,128}
            mouseSpd = -1
        else
            mouseColor = {0,255,0,128}
            mouseSpd = 0.5
        end
    end

    -- fade out in the borders
    if mx < screenSize.x * 0.1 or mx > screenSize.x * 0.9 or
        my < screenSize.y * 0.1 or my > screenSize.y * 0.9 then
        local frac = math.min(mx/(screenSize.x*0.1), (screenSize.x - mx)/(screenSize.x*0.1),
            my/(screenSize.y*0.1), (screenSize.y - my)/(screenSize.y*0.1))
        frac = math.max(0, frac)
        mouseColor[4] = 128 * frac
    end


    -- angle
    mouseAngle = (mouseAngle + mouseSpd * dt + math.pi * 2) % (math.pi*2)
end

function drawMousePointer()
    if not titlesActive then
        love.mouse.setVisible(true)
    else
        love.mouse.setVisible(false)
        love.graphics.setLineWidth(2)
        love.graphics.setPointSize(2)
        love.graphics.setColor(unpack(mouseColor))
        local mx,my = getMouseCoords()
        love.graphics.circle("line", mx, my, 20, 32)
        love.graphics.point(mx,my)
        local divs = 3
        for i=0,divs-1 do
            local vx = math.cos(mouseAngle + i * math.pi*2/divs)
            local vy = math.sin(mouseAngle + i * math.pi*2/divs)
            love.graphics.line(mx + vx * 15, my+vy*15, mx + vx * 25, my + vy * 25)
        end
    end
end