function initSound()
    sounds = {
        bgs = {
            love.audio.newSource("music/Alpha Boy - High Power Laser.ogg","stream"),
            love.audio.newSource("music/Alpha Boy - Zeros and Ones.ogg","stream"),
        },
        lazer = love.audio.newSource("music/laser gun.ogg"),
        current = 1,
    }
    sounds.current = math.random(#sounds.bgs)
end

function startSound()
    -- do return end
    sounds.bgs[sounds.current]:play()
end

function updateSound(dt)
    if not sounds.bgs[sounds.current]:isPlaying() then
        sounds.current = (sounds.current % #sounds.bgs) + 1
        startSound()
    end
end

function playLaserSound(hit)
    if sounds then
        if hit then sounds.lazer:play() else sounds.lazer:stop() end
    end
end