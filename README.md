ACE
===

An audio/visual experience

made at the Berlin Mini Game Jam September 2015

Credits
-------

    * code+graphics: Christiaan Janssen
    * music+soundfx: Norman Ritter
    * engine: Löve2D
    * font: LiquidCrystal by Chase Babb
    * license: Creative Commons 3.0 - Attribution - ShareAlike (CC-BY-SA 3.0)

Music
-----

    Alpha Boy - High Power Laser
    Alpha Boy - Zeros and Ones

    license: Copyright Alpha Boy 2014-2015, All rights reserved
